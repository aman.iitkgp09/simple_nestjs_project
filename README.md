### The Project is based on nestjs framework:

In my current project, I've diligently implemented all the requirements as requested. 
This involved creating both the docker-compose file and the Dockerfile to containerize my NestJS application. 
The use of these containerization tools has enhanced the scalability, portability, and deployment efficiency of the application, contributing to a streamlined and efficient development process.

#### Pre-requisite :
.env is already provided for convenience in the main directory of 'Simple_Nestjs_Project'.
Keep the provided ports available: 27017, 5432, and 3000

#### Run :

##### Command to the application: 
##### `docker-compose up`  

#### Access backend web: swagger link: `http://localhost:3000/api`

#### About the architecture.
As specified in the assignment, the product list database is MongoDB instances and the UserList database POSTgres. 
Once the application is up and running, users can access the Swagger documentation through the provided link (^).
#### User Flow

###### 1. Database Initialization:
The databases are presently devoid of data, and access to the APIs is limited.

###### 2. API Accessibility:
Only APIs listed under the 'Auth' header are accessible.

###### 3. Account Creation:
Users have the ability to create a new account, ensuring the uniqueness of both the username and password.

###### 4. JWT Token Generation:
Following the successful creation of a user account, logging in generates a JWT token. Please utilize the provided JWT token by including it as a header authorization.

###### 5. JWT Token Inclusion:
The lock icon located on the right side of each API is designated for the addition of the JWT token. Ensure that the provided token is appropriately included for access.

#### Functionality of App:
######  The App is a users personal cart, he/she can use it for storing new products.
######  All users can see each others product list and modify his list accordingly.
######  No other user can modify the list of other users.
###### Once user is delete his mapped products will also get deleted
###### Upon calling the GET/user/{username} API, users retrieve a list of all products mapped to his/her account

<img align="center" src="images/img.png" />