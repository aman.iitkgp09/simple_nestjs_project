import { Body, Controller, HttpStatus, Post, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersService } from 'src/users/service/users.service';
import { RegisterUser } from 'src/users/dto/register-user.dto';
import {
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
  ApiUnprocessableEntityResponse,
  ApiOperation,
} from '@nestjs/swagger';

@Controller('auth')
@ApiTags('Auth')
@ApiOkResponse({ description: 'The resource was returned successfully' })
@ApiForbiddenResponse({ description: 'Unauthorized Request' })
@ApiNotFoundResponse({ description: 'Resource not found' })
@ApiUnprocessableEntityResponse({ description: 'Bad Request' })
export class AuthController {
  constructor(
    private authService: AuthService,
    private usersService: UsersService,
  ) {}

  @Post('/login')
  @ApiOperation({
    description: 'Login',
  })
  async login(
    @Res() response,
    @Body() registerUser: RegisterUser,
  ): Promise<any> {
    try {
      const token = await this.authService.login(registerUser);
      return response.status(HttpStatus.CREATED).json({
        message: 'Login successful',
        token,
      });
    } catch (error) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: 'Error: Login Error!',
        error: 'Bad Request',
      });
    }
  }

  @Post('/signup')
  @ApiOperation({
    description: 'To Create new User',
  })
  async createUser(@Res() response, @Body() registerUser: RegisterUser) {
    try {
      const data = await this.usersService.create(registerUser);
      return response.status(HttpStatus.CREATED).json({
        message: 'User has been registered successfully',
        data,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: 'Error: User not Registered!',
        error: 'Bad Request',
      });
    }
  }
}
