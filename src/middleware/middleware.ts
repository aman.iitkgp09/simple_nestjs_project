import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction } from 'express';
import { decode } from 'jsonwebtoken';

export interface ExtendedHeaders extends Request {
  userobj: any;
}

@Injectable()
export class PayloadMiddleware implements NestMiddleware {
  use(req: Request & ExtendedHeaders, res: Response, next: NextFunction) {
    const authorizationHeader = req.headers['authorization'];

    if (authorizationHeader) {
      const token = authorizationHeader.split(' ')[1];
      if (token) {
        const jwtPayload = decode(token);
        req.userobj = jwtPayload;
      }
    }
    next();
  }
}
