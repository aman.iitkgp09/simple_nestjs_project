import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { CreateProductDTO } from '../dto/create-product.dto';
import { UpdateProductDTO } from '../dto/update-product.dto';
import { ProductService } from '../service/product.service';
import { AuthGuard } from '@nestjs/passport';
import { InputProductDTO } from '../dto/input-product.dto';

@ApiTags('Product')
@Controller('product')
@ApiOkResponse({ description: 'The resource was returned successfully' })
@ApiForbiddenResponse({ description: 'Unauthorized Request' })
@ApiNotFoundResponse({ description: 'Resource not found' })
@ApiUnprocessableEntityResponse({ description: 'Bad Request' })
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth('access-token')
  @ApiOperation({
    description: 'To Create new Product Category',
  })
  async createProduct(
    @Res() response,
    @Body() inputProductDTO: InputProductDTO,
    @Request() req: any,
  ) {
    try {
      const createProduct: CreateProductDTO = {
        name: inputProductDTO.name,
        price: inputProductDTO.price,
        description: inputProductDTO.description,
        quantity: inputProductDTO.quantity,
        mappedUser: req.userobj.username,
      };

      const data = await this.productService.create(createProduct);
      return response.status(HttpStatus.CREATED).json({
        message: 'Product has been created successfully',
        data,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: 'Error: Product not Created!',
        error: 'Bad Request',
      });
    }
  }

  @Get('/allproduct')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth('access-token')
  @ApiOperation({
    description: 'To get list of all Products',
  })
  async getAllProducts(@Res() response) {
    try {
      const data = await this.productService.getAll();
      return response.status(HttpStatus.OK).json({
        message: 'All Product data found successfully',
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/:productId')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth('access-token')
  @ApiParam({
    name: 'productId',
    required: true,
    description: 'To find existing product with productId',
    type: String,
  })
  @ApiOperation({
    description: 'To get product by productId',
  })
  async getProduct(@Res() response, @Param('productId') productId: string) {
    try {
      const data = await this.productService.getProductById(productId);
      return response.status(HttpStatus.OK).json({
        message: 'Product found successfully',
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Put('/:productId')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth('access-token')
  @ApiParam({
    name: 'productId',
    required: true,
    description: 'Add productId to be modified',
    type: String,
  })
  @ApiOperation({
    description:
      'The api is used to modify especific Product, found by productId',
  })
  async updateProduct(
    @Res() response,
    @Request() req: any,
    @Param('productId') productId: string,
    @Body() updateProductDTO: UpdateProductDTO,
  ) {
    const username = req.userobj.username;
    try {
      const data = await this.productService.updateProduct(
        productId,
        username,
        updateProductDTO,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Product has been successfully updated',
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:productId')
  @UseGuards(AuthGuard('jwt'))
  @ApiBearerAuth('access-token')
  @ApiParam({
    name: 'productId',
    required: true,
    description: 'To delete existing Product with productId',
    type: String,
  })
  @ApiOperation({
    description:
      'This api deletes exisiting Product from database, using productId',
  })
  async deleteCategory(
    @Res() response,
    @Request() req: any,
    @Param('productId') productId: string,
  ) {
    const username = req.userobj.username;
    try {
      const data = await this.productService.deleteProductById(
        productId,
        username,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Product deleted successfully',
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
