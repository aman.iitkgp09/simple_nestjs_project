import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateProductDTO } from '../dto/create-product.dto';
import { UpdateProductDTO } from '../dto/update-product.dto';
import { Product, ProductDocument } from '../schema/product.schema';

@Injectable()
export class ProductRepository {
  constructor(
    @InjectModel(Product.name)
    private productModel: Model<ProductDocument>,
  ) {}

  async create(createProductDTO: CreateProductDTO): Promise<ProductDocument> {
    const newProduct = new this.productModel(createProductDTO);
    try {
      const val = await newProduct.save();
      return val;
    } catch (err) {
      throw new ForbiddenException();
    }
  }

  async getAllProducts(): Promise<ProductDocument[]> {
    const productData = await this.productModel.find();
    if (!productData || productData.length == 0) {
      throw new NotFoundException('Product data not found');
    }
    return productData;
  }

  async getAllProductsByMappedUser(
    username: string,
  ): Promise<ProductDocument[]> {
    const productData = await this.productModel.find({
      mappedUser: username,
    });
    if (!productData || productData.length == 0) {
      throw new NotFoundException('Product data not found');
    }
    return productData;
  }

  async getProductById(productId: string): Promise<ProductDocument> {
    const productData = await this.productModel.findById(productId);
    if (!productData) {
      throw new NotFoundException(`Product #${productId} not found`);
    }
    return productData;
  }

  async updateProduct(
    productId: string,
    updateFields: UpdateProductDTO,
  ): Promise<ProductDocument> {
    const productData = await this.productModel.findByIdAndUpdate(
      productId,
      updateFields,
      { new: true, useFindAndModify: false },
    );
    if (!productData) {
      throw new NotFoundException(`Product #${productId} not found`);
    }
    return productData;
  }

  async deleteProduct(productId: string): Promise<ProductDocument> {
    const productData = await this.productModel.findByIdAndDelete(productId);
    if (!productData) {
      throw new NotFoundException(`Product #${productId} not found`);
    }
    return productData;
  }

  async deleteProducMappedByUserWithUsername(
    username: string,
  ): Promise<boolean> {
    try {
      await this.productModel.deleteMany({
        mappedUser: username,
      });
      return true;
    } catch (err) {
      console.error(err.message);
    }
  }
}
