import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type ProductDocument = Product & Document;

@Schema()
export class Product {
  @Prop()
  name: string;
  @Prop()
  price: number;
  @Prop()
  description: string;
  @Prop()
  mappedUser: string;
  @Prop()
  quantity: number;
}

export const ProductSchema = SchemaFactory.createForClass(Product);
