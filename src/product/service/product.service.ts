import { Injectable, NotAcceptableException } from '@nestjs/common';
import { CreateProductDTO } from '../dto/create-product.dto';
import { UpdateProductDTO } from '../dto/update-product.dto';
import { ProductRepository } from '../repository/product.repository';
import { Product } from '../schema/product.schema';

@Injectable()
export class ProductService {
  constructor(private productRepository: ProductRepository) {}

  async create(createProductDTO: CreateProductDTO): Promise<Product> {
    return await this.productRepository.create(createProductDTO);
  }

  async getAll(): Promise<Product[]> {
    return await this.productRepository.getAllProducts();
  }

  async getProductById(productId: string): Promise<Product> {
    return await this.productRepository.getProductById(productId);
  }

  async updateProduct(
    productId: string,
    username: string,
    updateProductDTO: UpdateProductDTO,
  ): Promise<Product> {
    const product = await this.productRepository.getProductById(productId);
    if (username === product.mappedUser) {
      return await this.productRepository.updateProduct(
        productId,
        updateProductDTO,
      );
    }
    throw new NotAcceptableException('Not Allowed');
  }

  async deleteProductById(
    productId: string,
    username: string,
  ): Promise<Product> {
    const product = await this.productRepository.getProductById(productId);
    if (username === product.mappedUser) {
      return await this.productRepository.deleteProduct(productId);
    }
    throw new NotAcceptableException('Not Allowed');
  }
}
