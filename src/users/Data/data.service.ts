// src/data/data.service.ts
import { Injectable } from '@nestjs/common';
import { UsersRepository } from '../repository/users.repository';
import { RegisterUser } from '../dto/register-user.dto';

@Injectable()
export class DataService {
  constructor(private readonly userRepository: UsersRepository) {}

  async populateDatabase(): Promise<void> {
    // Add logic to populate the database with initial data
    const user = new RegisterUser();
    const user1 = new RegisterUser();
    const user2 = new RegisterUser();
    const user3 = new RegisterUser();

    user.username = 'admin';
    user.password = 'adminpassword';
    await this.userRepository.create(user);
    user1.username = 'admin1';
    user1.password = 'adminpassword';
    await this.userRepository.create(user1);
    user2.username = 'admin2';
    user2.password = 'adminpassword';
    await this.userRepository.create(user2);
    user3.username = 'admin3';
    user3.password = 'adminpassword';
    await this.userRepository.create(user3);
  }
}
