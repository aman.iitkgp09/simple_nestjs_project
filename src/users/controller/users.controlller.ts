import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiForbiddenResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiTags,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { UsersService } from '../service/users.service';
import { UpdateUsersDTO } from '../dto/update-user.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('User')
@UseGuards(AuthGuard('jwt'))
@Controller('user')
@ApiOkResponse({ description: 'The resource was returned successfully' })
@ApiForbiddenResponse({ description: 'Unauthorized Request' })
@ApiNotFoundResponse({ description: 'Resource not found' })
@ApiUnprocessableEntityResponse({ description: 'Bad Request' })
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  // @Post('/signup')
  // @ApiOperation({
  //   description: 'To Create new User',
  // })
  // async createUser(@Res() response, @Body() registerUser: RegisterUser) {
  //   try {
  //     const data = await this.usersService.create(registerUser);
  //     return response.status(HttpStatus.CREATED).json({
  //       message: 'User has been registered successfully',
  //       data,
  //     });
  //   } catch (err) {
  //     return response.status(HttpStatus.BAD_REQUEST).json({
  //       statusCode: 400,
  //       message: 'Error: User not Registered!',
  //       error: 'Bad Request',
  //     });
  //   }
  // }

  @Get('/allusers')
  @ApiBearerAuth('access-token')
  @ApiOperation({
    description: 'To get list of all users',
  })
  async getAllUsers(@Res() response) {
    try {
      const data = await this.usersService.findAll();
      return response.status(HttpStatus.OK).json({
        message: 'All User data found successfully',
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/:username')
  @ApiBearerAuth('access-token')
  @ApiParam({
    name: 'username',
    required: true,
    description: 'To find existing user with username',
    type: String,
  })
  @ApiOperation({
    description: 'To get user by username',
  })
  async getUser(@Res() response, @Param('username') username: string) {
    try {
      const data = await this.usersService.findOne(username);
      return response.status(HttpStatus.OK).json({
        message: 'User found successfully',
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Patch('/:userId')
  @ApiBearerAuth('access-token')
  @ApiParam({
    name: 'userId',
    required: true,
    description: 'Add userId to be modified',
    type: String,
  })
  @ApiOperation({
    description: 'The api is used to modify especific User, found by userId',
  })
  async updateUser(
    @Res() response,
    @Request() req: any,
    @Param('userId') userId: number,
    @Body() updateUsersDTO: UpdateUsersDTO,
  ) {
    try {
      const username = req.userobj.username;
      const data = await this.usersService.update(
        userId,
        username,
        updateUsersDTO,
      );
      return response.status(HttpStatus.OK).json({
        message: 'User has been successfully updated',
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:userId')
  @ApiBearerAuth('access-token')
  @ApiParam({
    name: 'userId',
    required: true,
    description: 'To delete existing User with userId',
    type: Number,
  })
  @ApiOperation({
    description: 'This api deletes exisiting User from database, using userId',
  })
  async deleteCategory(
    @Res() response,
    @Request() req: any,
    @Param('userId') userId: number,
  ) {
    try {
      const username = req.userobj.username;
      const data = await this.usersService.remove(userId, username);
      return response.status(HttpStatus.OK).json({
        message: 'User deleted successfully',
        data,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
