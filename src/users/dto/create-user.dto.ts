import { IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUsersDTO {
  @IsString()
  @ApiProperty()
  @IsEmail()
  readonly email: string;

  @IsString()
  @ApiProperty()
  readonly username: string;

  @IsString()
  @ApiProperty()
  password: string;
}
