import { Product, ProductDocument } from 'src/product/schema/product.schema';

export class ReturnUsersDTO {
  email: string;
  username: string;
  createdAt: Date;
  updateAt: Date;
  productList: ProductDocument[];
}
