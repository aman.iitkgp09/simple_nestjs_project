import { PartialType } from '@nestjs/mapped-types';
import { CreateUsersDTO } from './create-user.dto';

export class UpdateUsersDTO extends PartialType(CreateUsersDTO) {}
