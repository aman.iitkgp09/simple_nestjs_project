import {
  ForbiddenException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { UsersEntity } from '../entity/users.entity';
import { UpdateUsersDTO } from '../dto/update-user.dto';
import { RegisterUser } from '../dto/register-user.dto';

@Injectable()
export class UsersRepository {
  constructor(
    @InjectRepository(UsersEntity) private userRepo: Repository<UsersEntity>,
  ) {}

  async create(registerUser: RegisterUser): Promise<UsersEntity> {
    try {
      const saltOrRounds = 10;
      registerUser.password = await bcrypt.hash(
        registerUser.password,
        saltOrRounds,
      );
      const usr = await this.userRepo.create(registerUser);
      try {
        return await this.userRepo.save(usr);
      } catch (error) {
        throw new ForbiddenException('Duplicate key');
      }
    } catch (error) {
      throw new ForbiddenException('Duplicate key');
    }
  }

  async getAllUsers(): Promise<UsersEntity[]> {
    try {
      return await this.userRepo.find();
    } catch (error) {
      throw new ForbiddenException('Not Allowed');
    }
  }

  async findUserByUsername(username: string): Promise<UsersEntity> {
    const userData = await this.userRepo.findOne({
      where: { username: username },
    });
    if (!userData) {
      throw new NotFoundException(`User #${username} not found`);
    }
    return userData;
  }

  async getUsersById(userId: number): Promise<UsersEntity> {
    const userData = await this.userRepo.findOne({ where: { id: userId } });
    if (!userData) {
      throw new NotFoundException(`User #${userId} not found`);
    }
    return userData;
  }

  async updateUser(
    userId: number,
    updateFields: UpdateUsersDTO,
  ): Promise<UsersEntity> {
    const { password } = updateFields;
    if (password) {
      updateFields.password = await bcrypt.hash(updateFields.password, 10);
    }
    const userData = await this.userRepo.update(userId, updateFields);
    if (!userData) {
      throw new NotFoundException(`Users #${userId} not found`);
    }
    return await this.userRepo.findOne({ where: { id: userId } });
  }

  async deleteUser(userId: number): Promise<boolean> {
    const userData = await this.userRepo.delete(userId);
    if (!userData) {
      throw new NotFoundException(`User #${userId} not found`);
    }
    return true;
  }
}
