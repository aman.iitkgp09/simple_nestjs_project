import { Injectable, NotAcceptableException } from '@nestjs/common';
import { UsersRepository } from '../repository/users.repository';
import { UsersEntity } from '../entity/users.entity';
import { RegisterUser } from '../dto/register-user.dto';
import { UpdateUsersDTO } from '../dto/update-user.dto';
import { ProductRepository } from 'src/product/repository/product.repository';
import { ReturnUsersDTO } from '../dto/return-user.dto';

@Injectable()
export class UsersService {
  constructor(
    private readonly userRepository: UsersRepository,
    private readonly productRepository: ProductRepository,
  ) {}

  async create(registerUser: RegisterUser): Promise<UsersEntity> {
    return await this.userRepository.create(registerUser);
  }

  async findAll(): Promise<UsersEntity[]> {
    return await this.userRepository.getAllUsers();
  }

  async findOne(username: string): Promise<ReturnUsersDTO> {
    const user = await this.userRepository.findUserByUsername(username);
    const val = await this.productRepository.getAllProductsByMappedUser(
      username,
    );
    const returnedUser: ReturnUsersDTO = {
      email: user.email,
      username: user.username,
      createdAt: user.createdAt,
      updateAt: user.updateAt,
      productList: val,
    };
    return returnedUser;
  }

  async findUserByUsername(userUsername: string): Promise<UsersEntity> {
    return this.userRepository.findUserByUsername(userUsername);
  }

  async update(
    id: number,
    username: string,
    updateUsersDTO: UpdateUsersDTO,
  ): Promise<UsersEntity> {
    const user = await this.userRepository.findUserByUsername(username);
    if (Number(user.id) === Number(id)) {
      return await this.userRepository.updateUser(id, updateUsersDTO);
    }
    throw new NotAcceptableException('Not Allowed');
  }

  async remove(id: number, username: string): Promise<boolean> {
    const user = await this.userRepository.findUserByUsername(username);
    console.log(user.id, id, user.id === id);
    if (Number(user.id) === Number(id)) {
      const deletedUser = await this.userRepository.deleteUser(id);
      await this.productRepository.deleteProducMappedByUserWithUsername(
        username,
      );
      return deletedUser;
    }
    throw new NotAcceptableException('Not Allowed');
  }
}
