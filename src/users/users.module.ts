import { Module } from '@nestjs/common';
import { UsersEntity } from './entity/users.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersRepository } from './repository/users.repository';
import { UsersService } from './service/users.service';
import { UsersController } from './controller/users.controlller';
import { ProductModule } from 'src/product/product.module';

@Module({
  imports: [TypeOrmModule.forFeature([UsersEntity]), ProductModule],
  controllers: [UsersController],
  providers: [UsersRepository, UsersService],
  exports: [UsersService],
})
export class UsersModule {}
